package sample;

import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;

public class Controller {

    public ImageView road;
    public int i = 0;
    public int j = 0;
    public ImageView road_2;
    public Pane paneRoad;
    public ImageView car;
    public ToggleGroup GroupOne;
    public RadioButton OneSpeed;
    public RadioButton TwoSpeed;
    public RadioButton ThreeSpeed;
    public Label Start;

    public void go(){
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                while (i < 500){
                    road.setTranslateY(i);
                    road_2.setTranslateY(j);
                    try {
                        if(OneSpeed.isSelected()){
                            Thread.sleep(3);
                        }else if (TwoSpeed.isSelected()){
                            Thread.sleep(2);
                        }else if (ThreeSpeed.isSelected()){
                            Thread.sleep(1);
                        }

                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    i++;
                    j++;
                    if (i==500){
                        i=0;
                        j=0;
                    }
                }
            }
        };
        Thread thread = new Thread(runnable);
        thread.start();
    }

    public void MouseClicked(MouseEvent mouseEvent) {

        Runnable runnable1 = new Runnable() {
            @Override
            public void run() {
                    car.setTranslateX(mouseEvent.getSceneX()-road.getFitWidth()/2);
                    if (mouseEvent.getX()<=50){
                        car.setTranslateX(mouseEvent.getX()-225);
                    }
                    if (mouseEvent.getX()>=450){
                        System.out.println(mouseEvent.getX());
                        car.setTranslateX(mouseEvent.getX()-275);
                    }
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
        };
        Thread thread1 = new Thread(runnable1);
        thread1.start();
    }

    public void Click(MouseEvent mouseEvent) {
        go();
    }

    public void Entered(MouseEvent mouseEvent) {
        Start.setScaleX(1.2);
        Start.setScaleY(1.2);
    }

    public void Exit(MouseEvent mouseEvent) {
        Start.setScaleX(1);
        Start.setScaleY(1);
    }
}

